<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## DevApp Pages Backend/Frontend

**DevApp** BACKEND/FRONTEND
### Tecnologías
 
  * [Laravel](https://laravel.com/) 5.8
  * [Webmin](https://www.djangoproject.com/)
  * [Porto](https://www.djangoproject.com/)
  * [DialogFlow](https://dialogflow.com/)
  * [Facebook Developers](https://developers.facebook.com/)
  * [Ngrok](https://ngrok.com/)
  * [Postgres](https://www.postgresql.org/)
  * [Vuejs-Fork](https://vuejs.org/)
  * [Angular-Fork](https://angular.io/) --> En Proceso

### Scrips para comandos ("Revisar MakeFile"), estos puede ejecutarlos con make "comando" ejemplo make run

### Crear Base de Datos en postgres "Actualmente se encuentra migrado en script bash se debe configurar las credenciales en el archivo config.txt"

  - `sudo su postgres`
  - `psql -c "DROP DATABASE dev_app"`
  - `psql -c "DROP USER dev_user"`
  - `psql -c "CREATE USER dev_user WITH ENCRYPTED PASSWORD '4444333221'"`
  - `psql -c "CREATE DATABASE dev_app WITH OWNER dev_user"`


  - `mysql -u root -p$'password-system' -e "DROP DATABASE dev_app;" 2> /dev/null`
  - `mysql -u root -p$'password-system' -e "CREATE DATABASE dev_app;" 2> /dev/null`

### Puede instalar dependencias con make install/ make db para la base de datos


DevApp Pagina es soportado por [@alfredynho](alfredynho.cg@gmail.com).


### Landing Page de la pagina
<p align="center"><img src="https://res.cloudinary.com/due8e2c3a/image/upload/v1560940292/alfredynho/page.png"></p>


### Dashboard --> Administrador del proyecto

<p align="center"><img src="https://res.cloudinary.com/due8e2c3a/image/upload/v1560940327/alfredynho/admin.png"></p>



### Terminal --> Funcionamiento del bot en terminal

<p align="center"><img src="https://res.cloudinary.com/due8e2c3a/image/upload/v1560940285/alfredynho/terminal_bot.png"></p>
