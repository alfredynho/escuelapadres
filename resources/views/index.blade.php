@extends('layouts.frontend')

@section('title', __('Escuela de Padres'))

@section('extracss')
    <style>
        #leaflet { height: 380px; }
        .custom .leaflet-popup-tip,
        .custom .leaflet-popup-content-wrapper {
            background:#02B095;
            color: #FFAF00;
        }
    </style>

    <style>
        #mapid { height: 380px; }
        .custom .leaflet-popup-tip,
        .custom .leaflet-popup-content-wrapper {
            background:#1A1A1A;
            color: #FFAF00;
        }
    </style>

@endsection

@section('content')

    <div class="brook-hero-nav-slider-area">
        @include('frontend.includes.slider')
    </div>

    <div class="brook-call-to-action bg_color--24 ptb--40">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-8 col-md-6 col-12">
                    <div class="call-content essential-cta-content text-center text-sm-left">
                        <h3 class="text-white wow move-up">Webinars Gratuitos para la Plataforma del Observatorio de Violencia Intrafamiliar</h3>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="call-btn text-center text-md-right mt_mobile--20 wow move-up">
                        <a class="brook-btn bk-btn-white text-theme btn-sd-size essential-btn" href="{{ route('inscripciones') }}"> Inscribirme</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="brook-profile-area ptb--100 ptb-md--80 ptb-sm--80 bg_color--1">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12 col-12">
                    <div class="thumb">
                        <img src="{{ asset('escuela/img/marcelo.png') }}" alt="about images">
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-12 col-12 mt_md--40 mt_sm--40">
                    <div class="bk-profile-content">
                        <h3 class="heading heading-h3">Marcelo Pablo Pacheco Camacho</h3>
                        <div class="bkseparator--40"></div>
                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="profile">
                                    <h5 class="heading heading-h5 theme-color">Biografia</h5>
                                    <div class="bkseparator--20"></div>
                                    <p class="bk_pra text-justify">
                                        - Trabajó como Docente a Tiempo Completo en Universidad Salesiana de Bolivia<br>
                                        - Estudió en Durham University <br>
                                        - Estudió en ISRP Paris <br>
                                        - Estudió en Universidad Católica Boliviana <br>
                                        - Estudió Psicología en Universidad Católica Boliviana "San Pablo" <br> <br>
                                        Es el primer psicólogo boliviano en obtener la beca Chevening de estudios de maestría en el Reino Unido. Actualmente trabaja como profesor universitario e investigador en las áreas de construcción de paz y neurociencia cognitiva.
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="profile">
                                    <h5 class="heading heading-h5 theme-color">Reseña</h5>
                                    <div class="bkseparator--20"></div>
                                        <p class="bk_pra text-justify">"Me apasiona la ciencia y el poder hacer que mis compatriotas vivan mejor. Valoro mucho la honestidad."</p>
                                </div>
                            </div>

                        </div>
                        <div class="mt--70 mt_md--40 mt_sm--40"></div>
                        <div class="social-share social--transparent">
                            <a href="https://www.facebook.com/marcelo.p.camacho.9" target="_blank"><i class="fab fa-facebook"></i></a>
                            <a href="https://www.youtube.com/watch?v=QocLs-4o-30" target="_blank"><i class="fab fa-youtube"></i></a>
                            <a href="https://www.pinterest.es/marcelopchc/" target="_blank"><i class="fab fa-pinterest"></i></a>
                            <a href="https://bo.linkedin.com/in/marcelo-pacheco-camacho-290376130" target="_blank"><i class="fab fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="brook-profile-area ptb--100 ptb-md--80 ptb-sm--80 bg_color--1">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12 col-12">
                    <div class="thumb">
                        <img src="{{ asset('escuela/img/marcos.png') }}" alt="marcos">
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-12 col-12 mt_md--40 mt_sm--40">
                    <div class="bk-profile-content">
                        <h3 class="heading heading-h3">Marcos Pérez Lamadrid</h3>
                        <div class="bkseparator--40"></div>
                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="profile">
                                    <h5 class="heading heading-h5 theme-color">Biografia</h5>
                                    <div class="bkseparator--20"></div>
                                    <p class="bk_pra text-justify">
                                        - Psicólogo clínico en Caja de Salud de la Banca Privada <br>
                                        - Trabaja en Instituto de Investigación,Interacción y Postgrado de Psicología-UMSA IIIPP <br>
                                        - Trabajó como Psicólogo de Primaria en Unidad Educativa del Ejercito La Paz <br>
                                        - Trabajó como Docente en Universidad Salesina de Bolivia <br>
                                        - Trabajó como Psicoterapeuta en Centro de Estudios Interdiciplinarios Comunitario CIEC <br>
                                        - Trabajó como Psicoterapeuta en Caja Nacional de Salud CNS <br>
                                        - Estudió Especialidad en Psicología Clínica en Universidad Católica Boliviana "San Pablo" <br>
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="profile">
                                    <h5 class="heading heading-h5 theme-color">Reseña</h5>
                                    <div class="bkseparator--20"></div>
                                    <p class="bk_pra text-justify">
                                        "Psicólogo practicante de Terapia Breve Centrada en Soluciones"
                                    </p>
                                </div>
                            </div>

                        </div>
                        <div class="mt--70 mt_md--40 mt_sm--40"></div>
                        <div class="social-share social--transparent">
                            <a href="https://www.facebook.com/kemarcos" target="_blank"><i class="fab fa-facebook"></i></a>
                            <a href="https://twitter.com/marcusronin" target="_blank"><i class="fab fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCqOE4dLubB6LsjAumzz77oQ" target="_blank"><i class="fab fa-youtube"></i></a>
                            <a href="https://bo.linkedin.com/in/marcos-p%C3%A9rez-lamadrid-121030138" target="_blank"><i class="fab fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bk-accordion-area ptb--150 ptb-md--80 ptb-sm--60 bg_color--1 basic-thine-line">
        <div class="container">
            <div class="row">

                <div class="col-lg-6">
                    <div class="bk-accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#" class="acc-btn" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                        Lección 1
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                        Criando a los hijos con soluciones
                                        El primer módulo estará enfocado en la crianza de los hijos e hijas enfocado en soluciones y el manejo del trauma, también se hace énfasis en brindar herramientas para mejorar la comunicación en los padres de familia como pareja.
                                        Bienvenida al curso <br><br>
                                        LECCION 1
                                        CRIANDO A LOS HIJOS CON SOLUCIONES PRIMERA PARTE <br> <br>
                                        En esta primera lección se hará énfasis en los superpoderes de los niños para ayudar a desarrollar una comunicación efectiva y ayudar a los padres a que puedan tener la mejor versión de ellos y de sus niños enfocándose en una convivencia pacífica.
                                        CRIANDO A LOS HIJOS CON SOLUCIONES SEGUNDA PARTE <br> <br>
                                        En la segunda parte de esta lección, continuando con el curso se profundizara en temas como apoyar a los niños en el hogar, a través del juego y el afecto, la importancia de la enseñar la responsabilidad a los hijos para que el hogar sea un lugar de protección y de cariño para ellos.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="bk-accordion" id="accordionExample2">
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <a href="#" class="acc-btn" data-toggle="collapse" data-target="#collapsefour"
                                        aria-expanded="false" aria-controls="collapsefour">
                                        Lección 2
                                    </a>
                                </h5>
                            </div>

                            <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExample2">
                                <div class="card-body">
                                    Conviértete en tu propio experto en relaciones de parejas
                                    La relación de pareja es importante para una estabilidad familiar, más aun es si la relación está orientada a la solución, para construir una relación responsable y de integridad para fortalecer la estabilidad familiar y que este sea útil para prevenir relaciones violentas en la familia.
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingfive">
                                <h5 class="mb-0">
                                    <a href="#" class="acc-btn collapsed" data-toggle="collapse" data-target="#collapsefive"
                                        aria-expanded="false" aria-controls="collapsefive">
                                        Leccion 3
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefive" class="collapse" aria-labelledby="headingfive" data-parent="#accordionExample2">
                                <div class="card-body">
                                    Soluciones para el trauma En la última lección se hace énfasis en la fuerza y el poder que tiene cada persona, en especial los adolescentes para salir de traumas y que estrategias tener frente a un trauma y brindar tácticas de supervivencia para generar esperanza ante eventos traumáticos.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="brook-icon-boxes-area ptb--100">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
                    <div class="icon-box text-center no-border bg-transparant vibrate-style">
                        <div class="inner">
                            <div class="icon">
                                <i class="fa fa-users"></i>
                            </div>
                            <div class="content">
                                <h5 class="heading heading-h5">Especialistas</h5>
                                <p>Cada conferencia es impartida por especialistas en diferentes materias: pediatras, psicólogos, ministros, pastores, profesores universitarios, entre otros.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
                    <div class="icon-box text-center no-border bg-transparant vibrate-style">
                        <div class="inner">
                            <div class="icon">
                                <i class="fa fa-clock"></i>
                            </div>
                            <div class="content">
                                <h5 class="heading heading-h5"> A tu propio ritmo</h5>
                                <p>Una vez registrado en el curso, podrás ver las videoconferencias a tu propio ritmo, según la disponibilidad de tu tiempo</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
                    <div class="icon-box text-center no-border bg-transparant vibrate-style">
                        <div class="inner">
                            <div class="icon">
                                <i class="fa fa-globe"></i>
                            </div>
                            <div class="content">
                                <h5 class="heading heading-h5"> Desde donde te encuentres</h5>
                                <p>Con la modalidad online puedes accesar los cursos desde cualquier lugar del mundo en el dispositivo de tu preferencia.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6">
                    <div class="icon-box text-center no-border bg-transparant vibrate-style">
                        <div class="inner">
                            <div class="icon">
                                <i class="fa fa-podcast"></i>
                            </div>
                            <div class="content">
                                <h5 class="heading heading-h5"> Gratis</h5>
                                <p>Te puedes registrar totalmente gratis en los cursos y a tu alcance</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="service-area bg_color--5 ptb--150 ptb-md--80 ptb-sm--60">
        <div class="container">
            <div class="row align-content-center">
                <div class="col-lg-6 col-12">
                    <div class="bk-title--default text-left">
                        <div class="bkseparator--30"></div>
                        <h4 class="heading-40 fw-200">Curso virtual escuela de padres <br> “CRIANDO A LOS HIJOS CON SOLUCIONES”</h4>
                    </div>
                </div>
                <div class="col-lg-6 col-12 mt_md--30 mt_sm--30">
                    <div class="modern-service-content wow move-up">
                        <p class="text-justify">Como unidad de Interacción, el Observatorio tiene la necesidad de promover una construcción de paz en las familias y en la población boliviana como eje central en enfoque de factores de protección en relación al entorno familiar.</p>
                    </div>
                </div>
            </div>
            <div class="row mt--100 mt_md--50 mt_sm--30">
                <div class="col-xl-4 col-lg-6 col-md-6 mb_lg--30 mb_md--30 mb_sm--30">
                    <div class="single-svg-icon-box">
                        <div class="grid-overlay" style="background-image: url({{ asset('escuela/img/banner/icon1.png') }})"></div>
                        <div class="inner">
                            <div class="svg-icon" id="svg-icon-1" data-svg-icon="{{ asset('escuela/img/icons/basic_accelerator.svg') }}"></div>
                            <div class="content">
                                <h5 class="heading heading-h5"> Educación</h5>
                                <p>La plataforma educativa para padres y madres que quieren aprender para disfrutar educando.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-6 col-md-6 mb_lg--30 mb_md--30 mb_sm--30">
                    <div class="single-svg-icon-box">
                        <div class="grid-overlay" style="background-image: url({{ asset('escuela/img/banner/icon2.png') }})"></div>
                        <div class="inner">
                            <div class="svg-icon" id="svg-icon-2" data-svg-icon="{{ asset('escuela/img/icons/basic_archive_full.svg') }}"></div>
                            <div class="content">
                                <h5 class="heading heading-h5"> Cursos</h5>
                                <p>Aprende con los mejores cursos sobre escuela de padres</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="single-svg-icon-box">
                        <div class="grid-overlay" style="background-image: url({{ asset('escuela/img/banner/icon3.png') }})"></div>
                        <div class="inner">
                            <div class="svg-icon" id="svg-icon-3" data-svg-icon="{{ asset('escuela/img/icons/ecommerce_bag.svg') }}"></div>
                            <div class="content">
                                <h5 class="heading heading-h5"> Retos</h5>
                                <p>Te proponemos una serie de retos que te ayudarán a crecer como madre o padre.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="contact-area ptb--100 ptb-md--80 ptb-sm--60">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-5 col-lg-6">
                    <div class="contact-wrap">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="brook-section-title text-center mb--40">
                                    <h3 class="heading heading-h3 fw-200">Formulario de Inscripción</h3>
                                    <p class="mt--15">Inscripción al Curso de Escuela de Padres <br> Inicio del curso Lunes 16 de Diciembre 2019</p>

                                    <div class="col-lg-4 col-md-6 col-12">
                                        <div class="call-btn text-center">
                                            <a class="brook-btn text-theme btn-sd-size" href="http://unidadacademica5.psicologiaumsa.edu.bo/" target="_blank"> Ir a Plataforma Moodle</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="contact-form bg-input-one" >

                                    @if (Session::has('contactos'))
                                        <div class="col-lg-12 col-md-12">
                                            <div class="mb-200">
                                                <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                                <strong>Mensaje Enviado Exitosamente .. </strong></strong>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                    {!! Form::open(['route' => 'inscription.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                                        <div class="row">

                                            <div class="col-lg-12">
                                                <input name="firstname" type="text" value="{{ old('firstname') }}" placeholder="Nombre *" required>
                                            </div>

                                            <div class="col-lg-12 mt--20">
                                                <input name="paterno" type="text" value="{{ old('paterno') }}" placeholder="Apellido Paterno *" required>
                                            </div>

                                            <div class="col-lg-12 mt--20">
                                                <input name="materno" type="text" value="{{ old('materno') }}" placeholder="Apellido Materno *" required>
                                            </div>

                                            <div class="col-lg-12 mt--20">
                                                <input name="cedula" type="number" value="{{ old('cedula') }}" placeholder="Cédula *" required>
                                            </div>

                                            <div class="col-lg-12 mt--30">
                                                <input name="email" type="email" value="{{ old('email') }}" placeholder="Correo *" required>
                                            </div>

                                            <div class="col-lg-12 mt--30">
                                                <input name="phone" type="number" value="{{ old('phone') }}" placeholder="Celular *" required>
                                            </div>

                                            <div class="col-lg-12 mt--30">
                                                <input type="submit" value="Enviar Mensaje">
                                                <p class="form-messege"></p>
                                            </div>

                                        </div>
                                    {{Form::Close()}}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 offset-xl-1 col-lg-6 mt_md--30 mt_sm--30">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="brook-section-title text-left mb--40">
                                <h3 class="heading heading-h3 fw-200">Contactos</h3>
                            </div>
                        </div>
                    </div>

                    <div class="leaflet"></div>

                    <div class="row mt--40">

                        <div class="col-md-4 col-sm-6">
                            <div class="single-contact-address">
                                <h5 class="title">Dirección</h5>
                                <div class="address mt--10">
                                    <p>Avenida Villazón Nº 1995, Plaza del Bicentenario. Zona Central</p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="single-contact-address">
                                <h5 class="title">Información</h5>
                                <div class="mail-phone">
                                    <p><a href="#">observatorio30@gmail.com</a></p>
                                    <p><a href="#">2612521 – 2444218</a></p>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6">
                            <div class="single-contact-address">
                                <h5 class="title">Redes Sociales</h5>
                                <div class="social-share">
                                    <p><a href="https://www.facebook.com/Observatorio-de-Violencia-Intrafamiliar-1141815382614924/" target="_blank">Facebook</a></p>
                                    <p><a href="https://www.youtube.com/channel/UCSyeQyODRk-jSW5-r7G6ONw" target="_blank">Youtube</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


  @if (Session::has('inscription'))

      <!-- Pushbar -->
        <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
          <div class="btn-close">
             	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
          </div>
              <div class="contenedor">
                  <div class="main-title">
                      <div class="title-main-page">
                      <div class="container text-center">
                          <h5>{{ (Session::get('inscription')) }} su registro se realizo exitosamente su usuario y contraseña se formo como en el ejemplo (usuario:Paterno) -> Merida (Contraseña:Paterno+cedula) -> Merida+4277300 , asegurese de ingresar de manera correcta sus datos</h5>
                          <button data-pushbar-close class="brook-btn bk-btn-theme btn-sd-size btn-rounded space-between" href="#">Escuela de Padres</button>
                        </div>
                      </div>
                  </div>
                  <br>
                  </div>
              </div>
        </div>
      <!-- end pushbar -->
  @elseif (Session::has('errorIns'))

      <!-- Pushbar -->
        <div data-pushbar-id="pushbar-notification" class="pushbar from_bottom pushbar-notification">
          <div class="btn-close">
             	<button data-pushbar-close><i class="fa fa-times-circle"></i></button>
          </div>
              <div class="contenedor">
                  <div class="main-title">
                      <div class="title-main-page">
                      <div class="container text-center">
                          <h5>{{ (Session::get('error')) }} usted ya se registro anteriormente </h5>
                          <button data-pushbar-close class="brook-btn bk-btn-theme btn-sd-size btn-rounded space-between" href="#">Escuela de Padres</button>
                        </div>
                      </div>
                  </div>
                  <br>
                  </div>
              </div>
        </div>
      <!-- end pushbar -->
  @endif

@endsection

@section('extrajs')

@endsection('extrajs')
