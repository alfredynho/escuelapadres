<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="Incos La Paz" />
<meta name="description" content="Sistema pagina web INCOS" />
<meta name="author" content="@jessy / Jessica Katherine Espinoza" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<title>Inicio de Sesion</title>
    {{--  @include('dashboard.includes.statics.css')  --}}
</head>

<body>

 <div class="wrapper">

<div id="pre-loader">
    <img src="{{ asset('images/pre-loader/loader-01.svg') }}" alt="loader incos">
</div>

<section class="height-100vh d-flex align-items-center page-section-ptb login" style="background-image: url({{ asset('incos/img/fondo.webp') }});" >

  <div class="container">
     <div class="row justify-content-center no-gutters vertical-align">
       <div class="col-lg-4 col-md-6 login-fancy-bg bg" style="background-image: url(images/login-inner-bg.webp);">
         <div class="login-fancy">
          <h2 class="text-white mb-20">Incos La Paz</h2>
          <p class="mb-20 text-white">Instituto Comercial Superior de la Nación Teniente Armando de Palacios</p>
          <ul class="list-unstyled  pos-bot pb-30">
            <li class="list-inline-item"><a class="text-white" href="http://www.incoslapaz.com" target="_blank"> INCOS LA PAZ {{ now()->year }}</a> </li>
          </ul>
         </div>
       </div>
       <div class="col-lg-4 col-md-6 bg-white">

        <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="login-fancy pb-40 clearfix">
        <h3 class="mb-30">Inicio de Sesión</h3>
         <div class="section-field mb-20">
             <label class="mb-10" for="name">Correo Electronico* </label>
               <input id="email" type="email" class="web form-control @error('email') is-invalid @enderror" placeholder="correo Electronico" required autocomplete="email" autofocus name="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

            </div>
            <div class="section-field mb-20">
             <label class="mb-10" for="Password">Contraseña* </label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            <div class="section-field">
              <div class="remember-checkbox mb-30">
                 <input type="checkbox" class="form-control" name="two" id="two" />
                </div>
              </div>
            <button type="submit" class="btn btn-success">
                {{ __('Iniciar Sesion') }}
            </button>
          </div>
      </form>

        </div>
      </div>
  </div>

</section>

</div>
    {{--  @include('dashboard.includes.statics.js')  --}}
</body>
</html>
