@extends('layouts.frontend')

@section('title', __('Inicio de Sesion - INCOS'))

@section('extracss')

@endsection('extracss')

@section('content')

    <section class="page-header page-header-modern page-header-background page-header-background-sm overlay overlay-color-primary overlay-show overlay-op-8 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12 align-self-center p-static order-2 text-center">
                    <h1>Inicio de Sesión</h1>
                </div>
                <div class="col-md-12 align-self-center order-1">
                    <ul class="breadcrumb breadcrumb-light d-block text-center">
                        <li><a href="#">INCOS</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

<section class="height-100vh d-flex align-items-center page-section-ptb login" style="background-image: url({{ asset('incos/img/fondo.webp') }});" >

  <div class="container">
     <div class="row justify-content-center no-gutters vertical-align">
        <div class="featured-boxes">
            <div class="row">
                <div class="col-md-12">
                    <div class="featured-box featured-box-primary text-center mt-5">
                        <div class="box-content">
                            <div class="testimonial-author">
                                <img src="{{ asset('incos/img/memorias.webp') }}" class="img-fluid rounded-circle mb-0" width="120" heigth="120" alt="Login INCOS">
                            </div>

                            <h4 class="color-primary font-weight-semibold text-4 text-uppercase mb-3">Inicio de Sesión</h4>
                            <form class="needs-validation" method="POST" action="{{ route('login') }}">
                            @csrf
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label class="font-weight-bold text-dark text-2"> Correo Electrónico</label>
                                        <input type="email" id="email" class="form-control form-control-lg @error('email') is-invalid @enderror" required autocomplete="email" autofocus name="email">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label class="font-weight-bold text-dark text-2">Contraseña</label>
                                        <input type="password" id="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-row">
                                    <button type="submit" class="btn btn-primary btn-modern float-right">
                                        {{ __('Iniciar Sesion') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>

        </div>
      </div>
  </div>

</section>
@endsection('contend')

@section('extrajs')

@endsection('extrajs')
