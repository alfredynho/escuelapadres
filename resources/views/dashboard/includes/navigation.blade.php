		<div class="navbar-wrapper  ">
			<div class="navbar-content scroll-div " >

				<div class="">
		            @include('dashboard.includes.user')
				</div>

				<ul class="nav pcoded-inner-navbar ">
					<li class="nav-item pcoded-menu-caption">
						<label>Menu de Opciones</label>
					</li>

					<li class="nav-item"><a href="{{ route('dashboard') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a></li>

					<li class="nav-item"><a href="{{ route('dashboard.blog.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-clipboard"></i></span><span class="pcoded-mtext">Blog</span></a></li>

					<li class="nav-item"><a href="{{ route('dashboard.carrera.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layers"></i></span><span class="pcoded-mtext">Carreras</span></a></li>
					<li class="nav-item"><a href="{{ route('dashboard.programa.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-menu"></i></span><span class="pcoded-mtext">Programas</span></a></li>
                    <li class="nav-item"><a href="{{ route('dashboard.admision.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-navigation"></i></span><span class="pcoded-mtext">Admisiones</span></a></li>
                    <li class="nav-item"><a href="{{ route('dashboard.students.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-star"></i></span><span class="pcoded-mtext">Est. Destacados</span></a></li>

					{{--  <li class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layers"></i></span><span class="pcoded-mtext">Widget</span><span class="pcoded-badge badge badge-success">100+</span></a>
						<ul class="pcoded-submenu">
							<li><a href="widget-statistic.html">Statistic</a></li>
							<li><a href="widget-data.html">Data</a></li>
							<li><a href="widget-chart.html">Chart</a></li>
						</ul>
                    </li>  --}}

					<li class="nav-item pcoded-hasmenu">
						<a href="#!" class="nav-link "><span class="pcoded-micon"><i class="feather icon-layers"></i></span><span class="pcoded-mtext">Asistente Virtual</span></a>
						<ul class="pcoded-submenu">
							<li><a href="{{ route('admin-messenger') }}">Usuarios Messenger</a></li>
							<li><a href="{{ route('dashboard.incosbot.index') }}">Faqs</a></li>
							<li><a href="{{ route('dashboard.categoria.index') }}">Categorias</a></li>
						</ul>
					</li>

					<li class="nav-item"><a href="{{ route('dashboard.gallery.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-image"></i></span><span class="pcoded-mtext">Imagenes</span></a></li>
					<li class="nav-item"><a href="{{ route('dashboard.academics.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-star"></i></span><span class="pcoded-mtext">Autoridades</span></a></li>
					<li class="nav-item"><a href="{{ route('dashboard.memories.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-volume-1"></i></span><span class="pcoded-mtext">Frases Academicas</span></a></li>
					<li class="nav-item"><a href="{{ route('dashboard.calendar.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-calendar"></i></span><span class="pcoded-mtext">Calendario Academico</span></a></li>
					<li class="nav-item"><a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-bookmark"></i></span><span class="pcoded-mtext">Contactos</span></a></li>
					<li class="nav-item"><a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-upload"></i></span><span class="pcoded-mtext">Descargas</span></a></li>
					<li class="nav-item"><a href="{{ route('dashboard.social.index') }}" class="nav-link "><span class="pcoded-micon"><i class="feather icon-share-2"></i></span><span class="pcoded-mtext">Redes Sociales</span></a></li>
					<li class="nav-item"><a href="#" class="nav-link "><span class="pcoded-micon"><i class="feather icon-users"></i></span><span class="pcoded-mtext">Usuarios</span></a></li>

				</ul>


			</div>
		</div>
