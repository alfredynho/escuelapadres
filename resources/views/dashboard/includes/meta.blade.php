@if (!empty($meta_property))
    {{-- Etiquetas Meta Descripcion de Blog  --}}
    <meta property="og:title"         content="{{ $share->nombre }}" />
    <meta property="og:description"   content="{{ $share->descripcion }}" />
    <meta property="og:image"         content="{{ Storage::url('Carrera/'.$share->imagen) }}" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:url" content="{{ url()->full() }}" />
    <meta name="twitter:title" content="{{ $share->nombre }}" />

@elseif(!empty($meta_property) and $meta_property==true)
    {{-- Etiquetas Meta General  --}}
    <meta property="og:title"         content="Instituto Comercial Superior de la Nación Teniente Armando de Palacios" />
    <meta property="og:description"   content="Visita nuestra Página Web en https://incoslapaz.com INCOS - Instituto Comercial Superior de la Nación Teniente Armando de Palacios." />
    <meta property="og:image"         content="https://www.your-domain.com/path/image.jpg" />
@endif
