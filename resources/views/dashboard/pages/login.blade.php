<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="HTML5 Template" />
<meta name="description" content="Sistema pagina web INCOS" />
<meta name="author" content="@jessy / Jessica Katherine Espinoza" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>Inicio de Sesion</title>
    @include('dashboard.includes.statics.css')
</head>

<body>

 <div class="wrapper">

<div id="pre-loader">
    <img src="images/pre-loader/loader-01.svg" alt="">
</div>

<section class="height-100vh d-flex align-items-center page-section-ptb login" style="background-image: url(incos/img/fondo.png);" >
  <div class="container">
    <form method="POST" action="{{ url('/usuario-actualizar') }}">
     <div class="row justify-content-center no-gutters vertical-align">
       <div class="col-lg-4 col-md-6 login-fancy-bg bg" style="background-image: url(images/login-inner-bg.jpg);">
         <div class="login-fancy">
          <h2 class="text-white mb-20">INCOS LA PAZ</h2>
          <p class="mb-20 text-white">Instituto Comercial Superior de la Nación Teniente Armando de Palacios</p>
          <ul class="list-unstyled  pos-bot pb-30">
            <li class="list-inline-item"><a class="text-white"> INCOS La Paz {{ now()->year }}</a> </li>
          </ul>
         </div>
       </div>

        @csrf
        {{ method_field('PUT') }}
        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)

                        <li>{{ $error }}</li>

                    @endforeach
                </ul>
            </div>
        @endif
        @if(session('notificacion'))
            <div class="alert alert-success" role="alert">
                {{session('notificacion')}}
            </div>
        @endif

       <div class="col-lg-4 col-md-6 bg-white">
        <div class="login-fancy pb-40 clearfix">
        <h3 class="mb-30">Inicio de Sesion</h3>
         <div class="section-field mb-20">
             <label class="mb-10" for="name">Usuario* </label>
               <input id="name" class="web form-control" type="text" placeholder="Ingrese su Usuario" name="web" value="{{ old('nombre') ?? auth()->user()->name }}">
            </div>
            <div class="section-field mb-20">
             <label class="mb-10" for="Password">Contraseña* </label>
               <input id="Password" class="Password form-control" type="password" placeholder="Ingrese su contraseña" name="Password">
            </div>
            <div class="section-field">
              <div class="remember-checkbox mb-30">
                 <input type="checkbox" class="form-control" name="two" id="two" />
                 <label for="two">Recordarme</label>
                 <a href="#" class="float-right">Olvide mi Contraseña</a>
                </div>
              </div>
              <a href="#" class="button">
                <span>Iniciar Sesion</span>
                <i class="fa fa-check"></i>
             </a>
             <p class="mt-20 mb-0">Sistema de Gestión Web INCOS LA PAZ</p>
          </div>
        </div>

      </div>
  </div>
        </form>
</section>


</div>


    @include('dashboard.includes.statics.js')

</body>
</html>
