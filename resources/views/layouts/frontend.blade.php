<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="Escuela de padres, Posgrado de Psicologia">
	<meta name="author" content="@alfredynho , Alfredo Callizaya Gutierrez" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="img/icon.png">

    @include('frontend.includes.statics.css')
</head>

<body class="template-color-23 template-font-7 CerebriSans-font">

    {{--  <!-- Start Preloader  -->
        @include('frontend.includes.loader')
    <!-- End Preloader  -->  --}}

    <div id="wrapper" class="wrapper">
        <header class="br_header header-default bg_color--23 border-bottom black-logo--version haeder-fixed-width haeder-fixed-150 headroom--sticky header-mega-menu clearfix">
            @include('frontend.includes.header')
        </header>

        <div class="popup-mobile-manu popup-mobile-visiable">
            @include('frontend.includes.popup')
        </div>

        {{--  <!-- Start Toolbar -->
        <div class="demo-option-container">
            @include('frontend.includes.toolbar')
        </div>
        <!-- End Toolbar -->  --}}

        <main class="page-content">
            @yield('content')
        </main>

        <footer id="bk-footer" class="page-footer essential-footer bg_color--3 pl--150 pr--150 pl_lg--30 pr_lg--30 pl_md--30 pr_md--30 pl_sm--5 pr_sm--5">
            @include('frontend.includes.footer')
        </footer>
    </div>


    <!-- Js Files -->
    @include('frontend.includes.statics.js')

</body>

</html>
