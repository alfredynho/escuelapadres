<div class="brook-element-carousel slick-arrow-center slick-arrow-rounded lr-0"
    data-slick-options='{
    "slidesToShow": 1,
    "slidesToScroll": 1,
    "arrows": true,
    "infinite": true,
    "dots": false,
    "prevArrow": {"buttonClass": "slick-btn slick-prev", "iconClass": "ion ion-ios-arrow-back" },
    "nextArrow": {"buttonClass": "slick-btn slick-next", "iconClass": "ion ion-ios-arrow-forward" }}'>

    <div class="hero-item bg-image" data-bg="{{ asset('escuela/img/banner/uno.webp') }}">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="hero-content-3 left">
                        <h4>Observatorio <br>de Violencia Intrafamiliar<br> de la carrera de <br>psicología UMSA.</h4>
                        <a class="brook-btn btn-sd-size space-between btn-essential btn" href="#">Ver mas</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hero-item bg-image" data-bg="{{ asset('escuela/img/banner/dos.webp') }}">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="hero-content-3 left">
                        <h4>Observatorio <br>de Violencia Intrafamiliar<br> de la carrera de <br>psicología UMSA.</h4>
                        <a class="brook-btn btn-sd-size space-between btn-essential btn" href="#">Ver mas</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hero-item bg-image" data-bg="{{ asset('escuela/img/banner/tres.webp') }}">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="hero-content-3 left">
                        <h4>Observatorio <br>de Violencia Intrafamiliar<br> de la carrera de <br>psicología UMSA.</h4>
                        <a class="brook-btn btn-sd-size space-between btn-essential btn" href="#">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
