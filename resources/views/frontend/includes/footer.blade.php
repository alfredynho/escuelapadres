<div class="bk-footer-inner ptb--150 pt_sm--100">
    <div class="container">
        <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                <div class="footer-widget text-var--2">
                    <div class="logo">
                        <a href="index.html">
                            <img src="{{ asset('escuela/img/escuela_padres_footer.png') }}" alt="Escuela de padres">
                        </a>
                    </div>
                    <div class="footer-inner">
                        <p>Webinars Gratuitos para la Plataforma del Observatorio de Violencia</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 offset-lg-1 col-md-6 col-sm-6 col-12 mt_mobile--40">
                <div class="footer-widget text-var--2 menu--about">
                    <h2 class="widgettitle">Observatorio Psicologia <br> UMSA</h2>
                </div>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-4 col-12 mt_md--40 mt_sm--40 mt_mobile--40">
                <div class="footer-widget text-var--2 menu--about">
                    <h2 class="widgettitle">Escuela de padres</h2>
                </div>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-4 col-12 mt_md--40 mt_sm--40 mt_mobile--40">
                <div class="footer-widget text-var--2 menu--about"></div>
            </div>

            <div class="col-lg-2 col-md-4 col-sm-4 col-12 mt_md--40 mt_md--40 mt_sm--40">
                <div class="footer-widget text-var--2 menu--instagram">
                    <h2 class="widgettitle">Redes Sociales</h2>

                    <div class="social-share social--transparent text-white padding-10">
                        <a href="https://www.facebook.com/Observatorio-de-Violencia-Intrafamiliar-1141815382614924/" target="_blank"><i class="fab fa-facebook"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UCSyeQyODRk-jSW5-r7G6ONw" target="_blank"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
