<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="header__wrapper mr--0">

                <div class="header-left">
                    <div class="logo">
                        <a href="{{ route('landing') }}">
                            <img src="{{ asset('escuela/img/escuela_padres.png') }}" alt="escuela de padres">
                        </a>
                    </div>
                </div>

                <div class="mainmenu-wrapper d-none d-lg-block">
                    <nav class="page_nav">
                        <ul class="mainmenu">
                            <li class="lavel-1"><a href="{{ route('landing') }}"><span>Inicio</span></a></li>

                            <li class="lavel-1"><a href="{{ route('nosotros') }}"><span>Nosotros</span></a></li>

                            <li class="lavel-1"><a href="{{ route('escuela') }}"><span>Escuela</span></a></li>

                            <li class="lavel-1"><a href="{{ route('inscripciones') }}"><span>Inscripciones</span></a></li>

                            <li class="lavel-1"><a href="{{ route('contactos') }}"><span>Contactos</span></a></li>
                        </ul>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</div>
