<link rel="stylesheet" href="{{ asset('escuela/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('escuela/css/revoulation.css') }}">
<link rel="stylesheet" href="{{ asset('escuela/css/plugins.css') }}">

<link rel="stylesheet" href="{{ asset('escuela/css/style.css') }}">

<link rel="stylesheet" href="{{ asset('escuela/css/custom.css') }}">
<link rel="stylesheet" href="{{ asset('escuela/css/leaflet.css') }}">

<link rel="stylesheet" href="{{ asset('escuela/pushbar.css') }}">


    <style>
    .pushbar {
        background:#FFFFFF;
        padding: 20px;
    }

    .pushbar .btn-close button{
        background: none;
        color:#333333;
        border:none;
        cursor:pointer;
        font-size:20px;
    }

    .pushbar-notification .contenedor{
        padding-bottom: 0;
    }

    </style>
