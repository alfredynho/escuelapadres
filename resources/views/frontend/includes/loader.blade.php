<div id="page-preloader" class="page-loading clearfix">
    <div class="page-load-inner">
        <div class="preloader-wrap">
            <div class="wrap-2">
                <div class=""> <img src="{{ asset('escuela/img/icons/brook-preloader.gif') }}" alt="Escuela de Padres"></div>
            </div>
        </div>
    </div>
</div>
