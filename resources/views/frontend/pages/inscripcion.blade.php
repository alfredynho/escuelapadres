@extends('layouts.frontend')

@section('title', __('Inscripciones - Escuela de padres'))

@section('extracss')

@endsection('extracss')

@section('content')
    <main class="page-content">

        <div class="bk_contact_classic bg_color--1 ptb--160 ptb-md--80 ptb-sm--80">
            <div class="container">
                <div class="row">

                    <div class="col-lg-4 col-xl-4 col-md-6 col-12 col-sm-12 wow move-up">
                        <div class="classic-address text-center">
                            <h4 class="heading heading-h4">Escuela de Padres</h4>
                            <div class="desc mt--15">
                                <p class="bk_pra line-height-2-22">Webinars Gratuitos para la Plataforma del Observatorio de Violencia</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-xl-4 col-md-6 col-12 col-sm-12 wow move-up mt_sm--40">
                        <div class="classic-address text-center">
                            <h4 class="heading heading-h4">Nuestro Correo</h4>
                            <div class="desc mt--15">
                                <p class="bk_pra line-height-2-22">observatorio30@gmail.com</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-xl-4 col-md-6 col-12 col-sm-12 wow move-up mt_md--40 mt_sm--40">
                        <div class="classic-address text-center">
                            <h4 class="heading heading-h4">Redes Sociales</h4>
                            <div class="desc mt--15">
                                <ul class="social-icon icon-solid-rounded icon-size-medium text-center move-up wow">
                                    <li class="facebook"><a href="https://www.facebook.com/Observatorio-de-Violencia-Intrafamiliar-1141815382614924/" target="_blank" class="link" aria-label="Facebook"><i class="fab fa-facebook"></i></a></li>
                                    <li class="pinterest"><a href="https://www.youtube.com/channel/UCSyeQyODRk-jSW5-r7G6ONw" target="_blank" class="link" aria-label="YouTube"><i class="fab fa-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="brook-contact-form-area ptb--150 ptb-md--80 ptb-sm--60 bg_color--5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="brook-section-title text-center mb--40">
                            <h4 class="heading heading-h4">Formulario de Inscripción</h4>
                            <p class="mt--15">Inscripción al Curso de Escuela de Padres <br> Inicio del curso Lunes 16 de Diciembre 2019</p>
                        </div>

                        <div class="call-btn text-center">
                            <a class="brook-btn text-theme btn-sd-size" href="http://unidadacademica5.psicologiaumsa.edu.bo/" target="_blank"> Ir a Plataforma Moodle</a>
                        </div> <br> <br><br>

                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="contact-form" >

                            @if (Session::has('contactos'))
                                <div class="col-lg-12 col-md-12">
                                    <div class="mb-200">
                                        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                                        <strong>Mensaje Enviado Exitosamente .. </strong></strong>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            {!! Form::open(['route' => 'inscription.store','method'=>'POST','autocomplete'=>'off','accept-charset'=>'UTF-8','enctype'=>'multipart/form-data','files' => true]) !!}

                                <div class="row">

                                    <div class="col-lg-12">
                                        <input name="firstname" type="text" value="{{ old('firstname') }}" placeholder="Nombre *" required>
                                    </div>

                                    <div class="col-lg-12 mt--20">
                                        <input name="paterno" type="text" value="{{ old('paterno') }}" placeholder="Apellido Paterno *" required>
                                    </div>

                                    <div class="col-lg-12 mt--20">
                                        <input name="materno" type="text" value="{{ old('materno') }}" placeholder="Apellido Materno *" required>
                                    </div>

                                    <div class="col-lg-12 mt--20">
                                        <input name="cedula" type="number" value="{{ old('cedula') }}" placeholder="Cédula *" required>
                                    </div>

                                    <div class="col-lg-12 mt--30">
                                        <input name="email" type="email" value="{{ old('email') }}" placeholder="Correo *" required>
                                    </div>

                                    <div class="col-lg-12 mt--30">
                                        <input name="phone" type="number" value="{{ old('phone') }}" placeholder="Celular *" required>
                                    </div>

                                    <div class="col-lg-12 mt--30">
                                        <input type="submit" value="Enviar Mensaje">
                                        <p class="form-messege"></p>
                                    </div>

                                </div>

                            {{Form::Close()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>
@endsection('content')

@section('extrajs')


@endsection
