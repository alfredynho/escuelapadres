@extends('layouts.frontend')

@section('title', __('Cursos - Escuela de padres'))

@section('extracss')

@endsection('extracss')

@section('content')

<main class="page-content">

    <div class="brook-profile-area ptb--100 ptb-md--80 ptb-sm--80 bg_color--1">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12 col-12">
                    <div class="thumb">
                        <img src="{{ asset('escuela/img/marcelo.png') }}" alt="about images">
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-12 col-12 mt_md--40 mt_sm--40">
                    <div class="bk-profile-content">
                        <h3 class="heading heading-h3">Marcelo Pablo Pacheco Camacho</h3>
                        <div class="bkseparator--40"></div>
                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="profile">
                                    <h5 class="heading heading-h5 theme-color">Biografia</h5>
                                    <div class="bkseparator--20"></div>
                                    <p class="bk_pra text-justify">
                                        - Trabajó como Docente a Tiempo Completo en Universidad Salesiana de Bolivia<br>
                                        - Estudió en Durham University <br>
                                        - Estudió en ISRP Paris <br>
                                        - Estudió en Universidad Católica Boliviana <br>
                                        - Estudió Psicología en Universidad Católica Boliviana "San Pablo" <br> <br>
                                        Es el primer psicólogo boliviano en obtener la beca Chevening de estudios de maestría en el Reino Unido. Actualmente trabaja como profesor universitario e investigador en las áreas de construcción de paz y neurociencia cognitiva.
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="profile">
                                    <h5 class="heading heading-h5 theme-color">Reseña</h5>
                                    <div class="bkseparator--20"></div>
                                        <p class="bk_pra text-justify">"Me apasiona la ciencia y el poder hacer que mis compatriotas vivan mejor. Valoro mucho la honestidad."</p>
                                </div>
                            </div>

                        </div>
                        <div class="mt--70 mt_md--40 mt_sm--40"></div>
                        <div class="social-share social--transparent">
                            <a href="https://www.facebook.com/marcelo.p.camacho.9" target="_blank"><i class="fab fa-facebook"></i></a>
                            <a href="https://www.youtube.com/watch?v=QocLs-4o-30" target="_blank"><i class="fab fa-youtube"></i></a>
                            <a href="https://www.pinterest.es/marcelopchc/" target="_blank"><i class="fab fa-pinterest"></i></a>
                            <a href="https://bo.linkedin.com/in/marcelo-pacheco-camacho-290376130" target="_blank"><i class="fab fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="brook-profile-area ptb--100 ptb-md--80 ptb-sm--80 bg_color--1">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 col-md-12 col-12">
                    <div class="thumb">
                        <img src="{{ asset('escuela/img/marcos.png') }}" alt="marcos">
                    </div>
                </div>
                <div class="col-lg-6 offset-lg-1 col-md-12 col-12 mt_md--40 mt_sm--40">
                    <div class="bk-profile-content">
                        <h3 class="heading heading-h3">Marcos Pérez Lamadrid</h3>
                        <div class="bkseparator--40"></div>
                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="profile">
                                    <h5 class="heading heading-h5 theme-color">Biografia</h5>
                                    <div class="bkseparator--20"></div>
                                    <p class="bk_pra text-justify">
                                        - Psicólogo clínico en Caja de Salud de la Banca Privada <br>
                                        - Trabaja en Instituto de Investigación,Interacción y Postgrado de Psicología-UMSA IIIPP <br>
                                        - Trabajó como Psicólogo de Primaria en Unidad Educativa del Ejercito La Paz <br>
                                        - Trabajó como Docente en Universidad Salesina de Bolivia <br>
                                        - Trabajó como Psicoterapeuta en Centro de Estudios Interdiciplinarios Comunitario CIEC <br>
                                        - Trabajó como Psicoterapeuta en Caja Nacional de Salud CNS <br>
                                        - Estudió Especialidad en Psicología Clínica en Universidad Católica Boliviana "San Pablo" <br>
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="profile">
                                    <h5 class="heading heading-h5 theme-color">Reseña</h5>
                                    <div class="bkseparator--20"></div>
                                    <p class="bk_pra text-justify">
                                        "Psicólogo practicante de Terapia Breve Centrada en Soluciones"
                                    </p>
                                </div>
                            </div>

                        </div>
                        <div class="mt--70 mt_md--40 mt_sm--40"></div>
                        <div class="social-share social--transparent">
                            <a href="https://www.facebook.com/kemarcos" target="_blank"><i class="fab fa-facebook"></i></a>
                            <a href="https://twitter.com/marcusronin" target="_blank"><i class="fab fa-twitter"></i></a>
                            <a href="https://www.youtube.com/channel/UCqOE4dLubB6LsjAumzz77oQ" target="_blank"><i class="fab fa-youtube"></i></a>
                            <a href="https://bo.linkedin.com/in/marcos-p%C3%A9rez-lamadrid-121030138" target="_blank"><i class="fab fa-linkedin"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bk-accordion-area ptb--150 ptb-md--80 ptb-sm--60 bg_color--1 basic-thine-line">
        <div class="container">
            <div class="row">

                <div class="col-lg-6">
                    <div class="bk-accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <a href="#" class="acc-btn" data-toggle="collapse" data-target="#collapseOne"
                                        aria-expanded="true" aria-controls="collapseOne">
                                        Lección 1
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                data-parent="#accordionExample">
                                <div class="card-body">
                                        Criando a los hijos con soluciones
                                        El primer módulo estará enfocado en la crianza de los hijos e hijas enfocado en soluciones y el manejo del trauma, también se hace énfasis en brindar herramientas para mejorar la comunicación en los padres de familia como pareja.
                                        Bienvenida al curso <br><br>
                                        LECCION 1
                                        CRIANDO A LOS HIJOS CON SOLUCIONES PRIMERA PARTE <br> <br>
                                        En esta primera lección se hará énfasis en los superpoderes de los niños para ayudar a desarrollar una comunicación efectiva y ayudar a los padres a que puedan tener la mejor versión de ellos y de sus niños enfocándose en una convivencia pacífica.
                                        CRIANDO A LOS HIJOS CON SOLUCIONES SEGUNDA PARTE <br> <br>
                                        En la segunda parte de esta lección, continuando con el curso se profundizara en temas como apoyar a los niños en el hogar, a través del juego y el afecto, la importancia de la enseñar la responsabilidad a los hijos para que el hogar sea un lugar de protección y de cariño para ellos.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="bk-accordion" id="accordionExample2">
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <a href="#" class="acc-btn" data-toggle="collapse" data-target="#collapsefour"
                                        aria-expanded="false" aria-controls="collapsefour">
                                        Lección 2
                                    </a>
                                </h5>
                            </div>

                            <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordionExample2">
                                <div class="card-body">
                                    Conviértete en tu propio experto en relaciones de parejas
                                    La relación de pareja es importante para una estabilidad familiar, más aun es si la relación está orientada a la solución, para construir una relación responsable y de integridad para fortalecer la estabilidad familiar y que este sea útil para prevenir relaciones violentas en la familia.
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingfive">
                                <h5 class="mb-0">
                                    <a href="#" class="acc-btn collapsed" data-toggle="collapse" data-target="#collapsefive"
                                        aria-expanded="false" aria-controls="collapsefive">
                                        Leccion 3
                                    </a>
                                </h5>
                            </div>
                            <div id="collapsefive" class="collapse" aria-labelledby="headingfive" data-parent="#accordionExample2">
                                <div class="card-body">
                                    Soluciones para el trauma En la última lección se hace énfasis en la fuerza y el poder que tiene cada persona, en especial los adolescentes para salir de traumas y que estrategias tener frente a un trauma y brindar tácticas de supervivencia para generar esperanza ante eventos traumáticos.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection('content')

@section('extrajs')


@endsection
