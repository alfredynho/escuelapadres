@extends('layouts.frontend')

@section('title', __('Nosotros - Escuela de padres'))

@section('extracss')

@endsection('extracss')

@section('content')

    <main class="page-content">

        <div class="brook-whatdo-area ptb--100 ptb-md--80 ptb-sm--60 bg_color--1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bk-title--default text-left">
                            <h5 class="heading heading-h5 theme-color">Escuela de padres</h5>
                            <div class="bkseparator--30"></div>
                            <h3 class="heading heading-h3">Observatorio de Psicologia</h3>
                        </div>
                    </div>
                </div>
                <div class="row mt--40 mt_md--20 mt_sm--5">
                    <div class="col-xl-2 col-lg-2">
                        <div class="what-do mt--40 mt_md--5 mt_sm--5">
                            <div class="basic-separator line-3"></div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-12">
                        <div class="what-do mt--40">
                            <div class="content">
                                <h5 class="heading heading-h5 theme-color wow move-up">Escuela </h5>
                                <div class="bkseparator--20"></div>
                                <p class="bk_pra wow move-up text-justify">El observatorio pretende realizar Webinars gratuitos donde expertos en el área de Psicología    desde las neurociencias y la Terapia breve centrada en soluciones se pueda mejorar la calidad de crianza de los padres con sus hijos y su entorno familiar desde los factores de protección y de cuidado teniendo como base la evidencia científica desde el estudio del cerebro, manejando datos reales desde nuestra ciencia la Psicologia y poder difundirla a la comunidad.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6 col-sm-12 offset-xl-1">
                        <div class="what-do mt--40">
                            <div class="content">
                                <h5 class="heading heading-h5 theme-color wow move-up">Los padres</h5>
                                <div class="bkseparator--20"></div>
                                <p class="bk_pra wow move-up text-justify">Muchos padres y Madres de familia muchas veces están preocupados por no encontrar una guía para mejorar la calidad de crianza y apego con sus hijos, no se tiene un libro o un manual  exacto de que hacer y qué no hacer como padres de familia o realmente se puede mejorar la relación en el entorno familiar para romper el ciclo de la violencia, el desconocimiento de las consecuencias de la violencia o la carencia de atención y afecto hacia los hijos perjudicarían en el desarrollo saludable del niño, niña adolescente.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="brook-testimonial-area bg_color--7">
            <div class="row row--0 align-items-center">
                <div class="col-lg-3 col-xl-6 text-center ptb-md--80 ptb-sm--80">
                    <div class="brook-section-title text-left title-max-width plr_sm--30 plr_md--40">
                        <h3 class="heading heading-h3 text-white">Escuela <br> de <br> Padres</h3>
                    </div>
                </div>

                <div class="col-lg-9 col-xl-6">
                    <div class="brook-element-carousel slick-arrow-center slick-arrow-triggle slick-arrow-trigglestyle-2 testimonial-space-right testimonial-color-variation"
                        data-slick-options='{
                                "spaceBetween": 0,
                                "slidesToShow": 2,
                                "slidesToScroll": 1,
                                "arrows": true,
                                "infinite": true,
                                "centerMode":true,
                                "dots": false,
                                "prevArrow": {"buttonClass": "slick-btn slick-prev", "iconClass": "fas fa-angle-left" },
                                "nextArrow": {"buttonClass": "slick-btn slick-next", "iconClass": "fas fa-angle-right" }
                            }'
                        data-slick-responsive='[
                                {"breakpoint":768, "settings": {"slidesToShow": 3}},
                                {"breakpoint":577, "settings": {"slidesToShow": 1}},
                                {"breakpoint":481, "settings": {"slidesToShow": 1}}
                            ]'>

                        <div class="testimonial testimonial_style--1 hover-transparent space-large--topbottom bg-dark">
                            <div class="content">
                                <p class="bk_pra">“Te puedes registrar totalmente gratis en los cursos o, si deseas”</p>
                                <div class="testimonial-info">
                                    <div class="post-thumbnail">
                                        <img src="{{ asset('escuela/img/logopadres.png') }}" alt="escuela de padres">
                                    </div>
                                </div>
                                <div class="testimonial-quote">
                                    <span class="fa fa-quote-right"></span>
                                </div>
                            </div>
                        </div>

                        <div class="testimonial testimonial_style--1 hover-transparent space-large--topbottom bg-dark">
                            <div class="content">
                                <p class="bk_pra">“Con la modalidad online puedes accesar los cursos desde cualquier lugar</p>
                                <div class="testimonial-info">
                                    <div class="post-thumbnail">
                                        <img src="{{ asset('escuela/img/logopadres.png') }}" alt="escuela de padres">
                                    </div>
                                </div>
                                <div class="testimonial-quote">
                                    <span class="fa fa-quote-right"></span>
                                </div>
                            </div>
                        </div>

                        <div class="testimonial testimonial_style--1 hover-transparent space-large--topbottom bg-dark">
                            <div class="content">
                                <p class="bk_pra">“Una vez registrado en el curso, podrás ver las videoconferencias”</p>
                                <div class="testimonial-info">
                                    <div class="post-thumbnail">
                                        <img src="{{ asset('escuela/img/logopadres.png') }}" alt="Escuela de padres">
                                    </div>
                                </div>
                                <div class="testimonial-quote">
                                    <span class="fa fa-quote-right"></span>
                                </div>
                            </div>
                        </div>

                        <div class="testimonial testimonial_style--1 hover-transparent space-large--topbottom bg-dark">
                            <div class="content">
                                <p class="bk_pra">“Cada conferencia es impartida por especialistas en diferentes materias:”</p>
                                <div class="testimonial-info">
                                    <div class="post-thumbnail">
                                        <img src="{{ asset('escuela/img/logopadres.png') }}" alt="Escuela de padres">
                                    </div>
                                </div>
                                <div class="testimonial-quote">
                                    <span class="fa fa-quote-right"></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="brook-call-to-action bg_color--2 ptb--70">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="call-content text-center text-sm-left">
                            <h3 class="heading heading-h3 text-white wow move-up">Webinars Gratuitos para la Plataforma del Observatorio de Violencia Intrafamiliar                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-12">
                        <div class="call-btn text-center text-sm-right mt_mobile--20 wow move-up">
                            <a class="brook-btn bk-btn-white text-theme btn-sd-size btn-rounded" href="#">Inscribirme</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </main>

@endsection

@section('extrajs')


@endsection
