<!DOCTYPE html>
<html>
	<head>

    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="keywords" content="INCOS" />
	<meta name="description" content="DevLaravel Template" />
	<meta name="author" content="jessi.com" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <title>INCOS - La Paz</title>	

    <link rel="shortcut icon" href="incos/img/favicon.png" type="image/x-icon" />
	<link rel="apple-touch-icon" href="incos/img/apple-touch-icon.png">
	
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="incos/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="incos/vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="incos/vendor/animate/animate.min.css">
    <link rel="stylesheet" href="incos/vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="incos/vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="incos/vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="incos/vendor/magnific-popup/magnific-popup.min.css">

    <link rel="stylesheet" href="incos/css/theme.css">
    <link rel="stylesheet" href="incos/css/theme-elements.css">
    <link rel="stylesheet" href="incos/css/theme-blog.css">
    <link rel="stylesheet" href="incos/css/theme-shop.css">

    <link rel="stylesheet" href="incos/vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="incos/vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="incos/vendor/rs-plugin/css/navigation.css">
    <link rel="stylesheet" href="incos/vendor/circle-flip-slideshow/css/component.css">
    
    <link rel="stylesheet" href="incos/css/skins/default.css"> 

    <link rel="stylesheet" href="incos/css/custom.css">

    <script src="incos/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body>
		<div class="body">
            <header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 45, 'stickySetTop': '-45px', 'stickyChangeLogo': true}">
				<div class="header-body">
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-row">
									<div class="header-logo">
										<a href="index.html">
											<img alt="Incos" width="56" height="62" data-sticky-width="32" data-sticky-height="40" data-sticky-top="25" src="incos/img/incos_logo.png">
										</a>
									</div>
								</div>
							</div>
							<div class="header-column justify-content-end">
								<div class="header-row pt-3">
									<nav class="header-nav-top">
										<ul class="nav nav-pills">
											<li class="nav-item nav-item-left-border nav-item-left-border-remove nav-item-left-border-md-show">
												<span class="ws-nowrap"><i class="fas fa-at"></i> incos@info.com</span>
                                            </li>
                                            
											<li class="nav-item nav-item-left-border nav-item-left-border-remove nav-item-left-border-md-show">
												<span class="ws-nowrap"><i class="fas fa-phone"></i> 2373296</span>
											</li>

										</ul>
									</nav>
								</div>
								<div class="header-row">
									<div class="header-nav pt-1">
										<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
											<nav class="collapse">
												<ul class="nav nav-pills" id="mainNav">
													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle active" href="index.html">
															Inicio
														</a>
                                                    </li>
                                                    
													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle" href="#">
															Institucional
														</a>
														<ul class="dropdown-menu">
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Historia</a>
                                                            </li>
                                                            
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Misión</a>
                                                            </li>
                                                            
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Visión</a>
                                                            </li>
                                                            
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Valores</a>
                                                            </li>
                                                            
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Objetivos</a>
                                                            </li>
                                                            
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Estructura Orgánica</a>
															</li>

														</ul>
                                                    </li>
                                                    
													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle" href="#">
															Carreras
														</a>
														<ul class="dropdown-menu">
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Contaduria General</a>
															</li>
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Secretariado Ejecutivo</a>
															</li>
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Comercio Exterior</a>
															</li>
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Sistemas Informaticos</a>
                                                            </li>
                                                            
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Linguistica</a>
															</li>

														</ul>
                                                    </li>
                                                    
													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle" href="#">
															Blog
														</a>
														<ul class="dropdown-menu">
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Noticias</a>
															</li>
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Articulos</a>
															</li>
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Actividades</a>
															</li>
														</ul>
                                                    </li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle" href="#">
															Programas
														</a>
														<ul class="dropdown-menu">
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Pasantias</a>
															</li>
															<li class="dropdown-submenu">
																<a class="dropdown-item" href="#">Trabajo</a>
															</li>
														</ul>
                                                    </li>
                                                    
													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle" href="index.html">
															Faqs
														</a>
                                                    </li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle" href="index.html">
															Imagenes
														</a>
                                                    </li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle" href="index.html">
															Contactos
														</a>
                                                    </li>

													<li class="dropdown">
														<a class="dropdown-item dropdown-toggle" href="index.html">
															Descargas
														</a>
                                                    </li>

												</ul>
											</nav>
										</div>
										<ul class="header-social-icons social-icons d-none d-sm-block">
											<li class="social-icons-facebook"><a href="https://www.facebook.com/Incoslapaz2019" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
											<li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UCbdUTCXX6wob4hv2d7SDF5w" target="_blank" title="YouTube"><i class="fab fa-youtube"></i></a></li>
										</ul>
										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
											<i class="fas fa-bars"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div role="main" class="main">
				@yield('content')
			</div>
 
			<footer id="footer">
				<div class="container">
					<div class="footer-ribbon">
						<span>INCOS - CONTACTOS</span>
					</div>
					<div class="row py-5 my-4">
						<div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
							<h5 class="text-3 mb-3">INCOS</h5>
							<p class="pr-1">Instituto Comercial Superior de la Nación Teniente Armando de Palacios</p>
							<div class="alert alert-success d-none" id="newsletterSuccess">
								<strong>Success!</strong> email list.
							</div>
							<div class="alert alert-danger d-none" id="newsletterError"></div>
							<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST" class="mr-4 mb-3 mb-md-0">
						</div>
						<div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
							<h5 class="text-3 mb-3">Objetivo</h5>
							<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options="{'username': 'oklerthemes', 'count': 2}">
								<p> Promover una educación superior integral y comunitaria, capaz de formar profesionales de excelencia con principios y valores éticos comprometidos con el desarrollo de la sociedad y el Estado.</p>
							</div>
						</div>
						<div class="col-md-6 col-lg-3 mb-4 mb-md-0">
							<div class="contact-details">
								<h5 class="text-3 mb-3">Contactos</h5>
								<ul class="list list-icons list-icons-lg">
									<li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i><p class="m-0">Dirección Calle Campero # 1035, esquina Federico Suazo</p></li>
									<li class="mb-1"><i class="fab fa-whatsapp text-color-primary"></i><p class="m-0"><a href="tel:8001234567">2373296</a></p></li>
									<li class="mb-1"><i class="far fa-envelope text-color-primary"></i><p class="m-0"><a href="mailto:mail@example.com">incos@info.com</a></p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-6 col-lg-2">
							<h5 class="text-3 mb-3">Redes Sociales</h5>
							<ul class="social-icons">
								<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
								<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
								<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container py-2">
						<div class="row py-4">
							<div class="col-lg-1 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
								<a href="index.html" class="logo pr-0 pr-lg-3">
									<img alt="Porto Website Template" src="incos/img/logo-footer.png" class="opacity-5" height="33">
								</a>
							</div>
							<div class="col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-start mb-4 mb-lg-0">
								<p>© {{ now()->year }}</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<script src="incos/vendor/jquery/jquery.min.js"></script>
		<script src="incos/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="incos/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="incos/vendor/jquery.cookie/jquery.cookie.min.js"></script>
		<script src="incos/vendor/popper/umd/popper.min.js"></script>
		<script src="incos/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="incos/vendor/common/common.min.js"></script>
		<script src="incos/vendor/jquery.validation/jquery.validate.min.js"></script>
		<script src="incos/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
		<script src="incos/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="incos/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="incos/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="incos/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="incos/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="incos/vendor/vide/jquery.vide.min.js"></script>
		<script src="incos/vendor/vivus/vivus.min.js"></script>
		<script src="incos/js/theme.js"></script>
		<script src="incos/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="incos/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="incos/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
		<script src="incos/js/custom.js"></script>
		<script src="incos/js/theme.init.js"></script>
		<script src="incos/js/view.home.js"></script>
	</body>
</html>