@extends('layouts.admin')

@section('content')
    <div class="page-header">
        <div class="page-block">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <div class="page-header-title">
                        <h5 class="m-b-10">Dashboard sistema</h5>
                    </div>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="feather icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard Sistema</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-7 col-md-12">
            <div class="row">
                <div class="col-sm-6">
                <div class="card feed-card">
                    <div class="card-header">
                        <h5>Registros en Blog</h5>
                        <div class="card-header-right">
                            <div class="btn-group card-option">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-more-horizontal"></i>
                                </button>
                                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                    <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> Expandir</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                    <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> Colapsar</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                    <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> Refrescar</a></li>
                                    <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> Remover</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">

                        @if(count($list_blog) >= '1')
                            @foreach ($list_blog as $lt)
                                <div class="row m-b-30">
                                    <div class="col-auto p-r-0">
                                        <i class="feather icon-check-circle bg-c-blue feed-icon"></i>
                                    </div>
                                    <div class="col">
                                        <a href="#!">
                                            <h6 class="m-b-5">{{ Str::limit($lt->title,33) }} <span class="text-muted float-right f-13">{{ $lt->created_at->format('d-m-Y') }}</span></h6>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="mb-200">
                                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Sin Registros! en Blog </strong> Nos encontramos preparando el mejor contenido para ti <strong> INCOS LA PAZ</strong>
                                </div>
                            </div>
                        @endif

                        <div class="text-center">
                            <a href="{{ route('dashboard.blog.index') }}" class="b-b-primary text-primary">Ver todos los registros</a>
                        </div>
                    </div>
				</div>

                </div>

                <div class="col-sm-6">
                <div class="card task-card ">
                    <div class="card-header">
                        <h5>Usuarios Messenger</h5>
                        <div class="card-header-right">
                            <div class="btn-group card-option">
                                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="feather icon-more-horizontal"></i>
                                </button>
                                <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                    <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> Expandir</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                    <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> Colapsar</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                    <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> Refrescar</a></li>
                                    <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> Remover</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled task-list">
                            @if(count($messenger) >= '1')
                                @foreach ($messenger as $ms)
                                    <li>
                                        <i class="feather icon-check f-w-600 task-icon bg-c-green"></i>
                                        <p class="m-b-5">
                                            @if($ms->gender =='male')
                                                Masculino
                                            @else
                                                Femenino
                                            @endif
                                        </p>
                                        <h6 class="text-muted">{{ $ms->first_name }} {{ $ms->last_name }} <span class="text-c-blue">
                                    </li>
                                @endforeach
                            @else
                                <div class="mb-200">
                                    <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                    <strong>Sin Registros! en Blog </strong> Nos encontramos preparando el mejor contenido para ti <strong> INCOS LA PAZ</strong>
                                    </div>
                                </div>
                            @endif
                        </ul>
                    </div>
                </div>
                </div>

            </div>
        </div>

        <div class="col-lg-5 col-md-12">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-yellow">{{ $carrera->count() }}</h4>
                                    <h6 class="text-muted m-b-0">Carreras</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-bar-chart-2 f-28"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-yellow">
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <a href="{{ route('dashboard.carrera.index') }}">
                                        <p class="text-white m-b-0">Ver Modulo</p>
                                    </a>
                                </div>
                                <div class="col-3 text-right">
                                    <i class="feather icon-trending-up text-white f-16"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-green">{{ $c_messenger->count() }}</h4>
                                    <h6 class="text-muted m-b-0">Facebook Usuarios</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-file-text f-28"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-green">
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <a href="{{ route('admin-messenger') }}">
                                        <p class="text-white m-b-0">Ver Módulo</p>
                                    </a>
                                </div>
                                <div class="col-3 text-right">
                                    <i class="feather icon-trending-up text-white f-16"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-red">{{ $blog->count() }}</h4>
                                    <h6 class="text-muted m-b-0">Blog</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-clipboard f-28"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-red">
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <a href="{{ route('dashboard.blog.index') }}">
                                        <p class="text-white m-b-0">Ver Módulo</p>
                                    </a>
                                </div>
                                <div class="col-3 text-right">
                                    <i class="feather icon-trending-down text-white f-16"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-8">
                                    <h4 class="text-c-blue">{{ $gallery->count() }}</h4>
                                    <h6 class="text-muted m-b-0">Galeria</h6>
                                </div>
                                <div class="col-4 text-right">
                                    <i class="feather icon-image f-28"></i>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-c-blue">
                            <div class="row align-items-center">
                                <div class="col-9">
                                    <a href="{{ route('dashboard.gallery.index') }}">
                                        <p class="text-white m-b-0">Ver Módulo</p>
                                    </a>
                                </div>
                                <div class="col-3 text-right">
                                    <i class="feather icon-trending-down text-white f-16"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-xl-6 col-md-12">
            <div class="card table-card">
                <div class="card-header">
                    <h5>Registros en Galeria de Imagenes</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>

                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> Expandir</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> Colapsar</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> Refrescar</a></li>
                                <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> Remover</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="chk-option">
                                            <label class="check-task custom-control custom-checkbox d-flex justify-content-center done-task">
                                                <input type="checkbox" class="custom-control-input">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </div>
                                        Imagen
                                    </th>
                                    <th>Activo?</th>
                                    <th>Fecha Creación</th>
                                    <th class="text-right">Destacado?</th>
                                </tr>
                            </thead>
                            <tbody>

                                @if(count($gallery) >= '1')
                                    @foreach ($gallery as $ga)
                                        <tr>
                                            <td>
                                                <div class="d-inline-block align-middle">
                                                    <img src="{{ Storage::url('Gallery/'.$ga->image) }}" alt="user image" class="img-radius wid-40 align-top m-r-15">
                                                    <div class="d-inline-block">
                                                        <h6>{{ $ga->title }}</h6>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                @if($ga->is_published == 1 )
                                                    <span class="badge badge-light-success">SI</span>
                                                @else
                                                    <span class="badge badge-light-danger">NO</span>
                                                @endif
                                            </td>
                                            <td>{{ $ga->created_at->format('d-m-Y') }}</td>
                                            <td class="text-right">
                                                @if($ga->destacado == 1 )
                                                    <span class="badge badge-light-success">SI</span>
                                                @else
                                                    <span class="badge badge-light-danger">NO</span>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <div class="mb-200">
                                        <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                        <strong>Sin Registros! en Redes Galeria </strong> Nos encontramos preparando el mejor contenido para ti <strong> INCOS LA PAZ</strong>
                                        </div>
                                    </div>
                                @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-md-12">
            <div class="card latest-update-card">
                <div class="card-header">
                    <h5>Redes Sociales</h5>
                    <div class="card-header-right">
                        <div class="btn-group card-option">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="feather icon-more-horizontal"></i>
                            </button>
                            <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> Expandir</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> Colpasar</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> Refrescar</a></li>
                                <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> Remover</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="latest-update-box">

                        @if(count($social) >= '1')
                            @foreach ($social as $sc)
                                <div class="row p-t-30 p-b-30">
                                    <div class="col-auto text-right update-meta">
                                        <i class="feather icon-share-2 bg-c-green update-icon"></i>
                                    </div>
                                    <div class="col">
                                        <a href="{{ route('dashboard.social.index') }}">
                                            <h6>{{ $sc->name }}</h6>
                                        </a>
                                        <p class="text-muted m-b-0">{{ $sc->url }}</p>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="mb-200">
                                <div class="alert alert-warning alert-dismissible fade show text-center" role="alert">
                                <strong>Sin Registros! en Redes Sociales </strong> Nos encontramos preparando el mejor contenido para ti <strong> INCOS LA PAZ</strong>
                                    <div class="text-center">
                                        <a href="{{ route('dashboard.social.index') }}" class="btn  btn-success">Agregar Registros </a>
                                    </div>
                                </div>
                            </div>
                        @endif

                    </div>
                    @if(count($social) >= '1')
                        <div class="text-center">
                            <a href="{{ route('dashboard.social.index') }}" class="b-b-primary text-warning">Ver las redes Sociales</a>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
@endsection
