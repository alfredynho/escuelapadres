@extends('layouts.frontend')

@section('title', __('Página no encontrada 404'))

@section('extracss')

@endsection

@section('content')

    <section class="page-header page-header-classic">
        <div class="container">
            <div class="row">
                <div class="col p-static">
                    <h1 data-title-border>404 - Pagina no Encontrada</h1>
                </div>
            </div>
        </div>
    </section>

    <div class="container">

        <section class="http-error">
            <div class="row justify-content-center py-3">
                <div class="col-md-7 text-center">
                    <div class="http-error-main">
                        <h2>404!</h2>
                        <p>Lo sentimos, pero la página que estaba buscando no existe.</p>
                    </div>
                </div>
                <div class="col-md-4 mt-4 mt-md-0">
                    <h4 class="text-primary">Aquí hay algunos enlaces útiles.</h4>
                    <ul class="nav nav-list flex-column">
                        <li class="nav-item"><a class="nav-link" href="/">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link" href="/carrera">carrera</a></li>
                        <li class="nav-item"><a class="nav-link" href="/faqs">FAQs</a></li>
                        <li class="nav-item"><a class="nav-link" href="/contactos">Contactos</a></li>
                        <li class="nav-item"><a class="nav-link" href="/descargas">Descargas</a></li>
                    </ul>
                </div>
            </div>
        </section>

    </div>

@endsection
