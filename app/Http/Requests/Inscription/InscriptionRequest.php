<?php

namespace App\Http\Requests\Inscription;

use Illuminate\Foundation\Http\FormRequest;

class InscriptionRequest extends FormRequest
{
    public function messages()
    {
        return [
            'user' => 'Debe completar los datos',
            'user.unique' => 'El usuario ya registro anteriormente!',
            'firstname' => 'El nombre es requerido',
            'lastname' => 'El apellido es requerido',
            'email' => 'El email es requerido'
        ];
    }

    public function rules()
    {
        return [
            'user' => 'unique:inscriptions,user,'.$this->id,
            'firstname' => 'required',
            'lastname'  => 'required',
            'email' => 'required',
        ];
    }
}
