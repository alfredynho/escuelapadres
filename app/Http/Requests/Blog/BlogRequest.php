<?php

namespace App\Http\Requests\Blog;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    public function messages()
    {
        return [
            'title' => 'El título es requerido',
            'title.unique' => 'El título ingresado no puede ser usado ya esta registrado!',
            'title.min' => 'El titulo es muy corto se debe tener al menos 6 caracteres',
            'title.max' => 'El titulo es muy largo se debe tener un maximo de 50 caracteres',
            'description.min'=>'Descripción muy corta, debe tener por lo menos 6 digitos',
            'destacado' => 'Estado destacado es requerido',
            'status' => 'Estado registro es requerido',
            'category' => 'La categoria es requerido'
        ];
    }

    public function rules()
    {
        return [
            'title' => 'required|min:6|max:50|unique:blogs,title,'.$this->id,
            'description' => 'required|min:6',
            'slug'  => 'unique:blogs',
            'destacado' => 'required',
            'status' => 'required',
            'category' => 'required',
        ];
    }
}
