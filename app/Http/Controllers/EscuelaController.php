<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Inscription\InscriptionRequest;

use App\Inscription;
use App\Student;

use Session;

class EscuelaController extends Controller
{

    public function index(Request $request)
    {
        $alfredynho = "@alfredynho";

        return view('index',[
            'alfredynho' => $alfredynho
        ]);
    }

    public function nosotros(Request $request)
    {
        $nosotros = "nosotros";

        return view('frontend.pages.nosotros',[
            'nosotros' => $nosotros
        ]);
    }

    public function escuela(Request $request)
    {
        $escuela = "escuela";

        return view('frontend.pages.cursos',[
            'escuela' => $escuela
        ]);
    }

    public function inscripcion(Request $request)
    {
        $inscription = "inscription";

        return view('frontend.pages.inscripcion',[
            'inscripcion' => $inscription
        ]);
    }

    public function contactos(Request $request)
    {
        $contactos = "contactos";

        return view('frontend.pages.contactos',[
            'contactos' => $contactos
        ]);
    }


    public function store(Request $request)
    {
        if(Student::where('cedula',$request->get('cedula'))->first()){
            Session::flash('errorIns',' Advertencia '.$request->get('firstname').' '.$request->get('lastname'));
            return Redirect()->route('landing');
        }else{

            // ('830164013721', '13721', '8301640', NULL, NULL, '8301640', 'Aliaga+8301640'),

            // Datos para Registro
            $inscription = new Inscription();

            $inscription->user = str_replace('ñ','n',$request->get('paterno'));
            $inscription->firstname = strtolower($request->get('firstname'));
            $inscription->lastname = $request->get('paterno');
            $inscription->email = $request->get('email');

            $_lastname = mb_strtolower((str_replace(" ","",$request->get('paterno'))),'UTF-8');
            $lt = str_replace('ñ','n',$_lastname);
            $ced = str_replace(' ','',$request->get('cedula'));

            $inscription->password = (ucfirst($lt).'+'.$ced);
            $inscription->email = $request->get('email');

            $student = new Student();

            // para mostrar

            $s_user = mb_strtolower((str_replace(" ","",$request->get('firstname'))),'UTF-8');
            $s_pass = (ucfirst($lt).'+'.$ced);

            $student->cedula = $request->get('cedula');

            $student->firstname = $request->get('firstname');
            $student->paterno = $request->get('paterno');
            $student->materno = $request->get('materno');
            $student->email = $request->get('email');
            $student->phone = $request->get('phone');
            $student->cedula = $request->get('cedula');
            $student->ref_cedula = $request->get('cedula');

            $student->user = mb_strtolower((str_replace(" ","",$request->get('paterno'))),'UTF-8');

            $student->save();

            $inscription->save();

            Session::flash('inscription','En hora buena '.$student->firstname. ' '. $student->lastname);
            return Redirect()->route('landing');
        }

    }

}
