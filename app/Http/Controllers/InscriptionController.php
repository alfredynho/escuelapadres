<?php

use App\Inscription;
use App\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Blog\BlogRequest;

use Session;

class InscriptionController extends Controller
{
    // public function index(Request $request)
    // {
    //     $blog = Blog::orderBy('id','desc')->get();

    //     //statistics dashboard
    //     $b_activos = Blog::where('is_published','=','1')->count();
    //     $b_destacados = Blog::where('destacado','=','1')->count();
    //     $b_t_blog = Blog::where('category','=','BLOG')->count();
    //     $b_t_noticia = Blog::where('category','=','NOTICIA')->count();
    //     $b_t_anuncio = Blog::where('category','=','ANUNCIO')->count();

    //     return view('dashboard.pages.blog.index', [
    //         'blog' => $blog,
    //         'b_activos'=> $b_activos,
    //         'b_destacados' => $b_destacados,
    //         'b_t_blog' => $b_t_blog,
    //         'b_t_noticia' => $b_t_noticia,
    //         'b_t_anuncio' => $b_t_anuncio

    //     ]);
    // }


    public function store(Request $request)
    {
        // ('830164013721', '13721', '8301640', NULL, NULL, '8301640', 'Aliaga+8301640'),

        // Datos para Estudiante

        $student = new Student();

        $student->firstname = $request->get('firstname');
        $student->lastname = $request->get('lastname');
        $student->email = $request->get('email');
        $student->phone = $request->get('phone');
        $student->cedula = $request->get('cedula');
        $student->save();

        // Datos para Registro
        $inscription = new Inscription();

        $inscription->user = mb_strtolower((str_replace(" ","",$request->get('lastname'))),'UTF-8');;
        $inscription->firstname = mb_strtolower((str_replace(" ","",$request->get('firstname'))),'UTF-8');;
        $inscription->lastname = mb_strtolower((str_replace(" ","",$request->get('lastname'))),'UTF-8');
        $inscription->email = $request->get('email');

        $_lastname = mb_strtolower((str_replace(" ","",$request->get('lastname'))),'UTF-8');
        $lt = str_replace('ñ','n',$_lastname);
        $ced = str_replace(' ','',$request->get('cedula'));

        $inscription->password = ($lt.'+'.$ced);
        $inscription->email = $request->get('email');

        $inscription->save();

        //Session::flash('create','Registro creado con Éxito');
        return Redirect()->route('index');
    }


    // public function update(BlogRequest $request, $id)
    // {
    //     $blog = Blog::findOrFail($id);

    //     $blog->title = $request->input('title');
    //     $blog->description = $request->input('description');
    //     $_imgCategory = $blog->category = $request->input('category');

    //     $blog->creador = auth()->user()->id;
    //     $_slug = mb_strtolower((str_replace(" ","-",$request->title)),'UTF-8');
    //     $blog->slug = preg_replace('/[^A-Za-z0-9\-]/', '', $_slug);

    //     $blog->is_published = $request->input('status');
    //     $blog->destacado = $request->input('destacado');

    //     $blog->update();

    //     Session::flash('update','Registro modificado con Éxito');
    //     return Redirect()->route('dashboard.blog.index');
    // }


	// public function destroy($id)
    // {
    //     $_blog = Blog::findOrFail($id);

    //     Session::flash('delete','Registro '.$_blog->title.' eliminado con Éxito');

    //     $_blog->delete();

    //     return Redirect()->route('dashboard.blog.index');
    // }
}
