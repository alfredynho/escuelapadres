<?php

namespace App\Http\Controllers\Dashboard;


use App\Blog;
use App\Gallery;
use App\Social;
use App\Messenger;
use App\Carrera;

// use App\Models\Event;
// use App\Models\Suscription;
// use App\Models\Inscription;


use App\Http\Controllers\Controller;

class DashboardController extends Controller
{

    public function index(){

        $blog = Blog::orderBy('id','desc')->get();

        $gallery = Gallery::orderBy('id','desc')->get();

        $list_blog = Blog::orderBy('id', 'asc')->take(4)->get();

        $social = Social::orderBy('id','asc')->get();

        $c_messenger = Messenger::orderBy('id','asc')->take(4)->get();

        $messenger = Messenger::orderBy('id','asc')->get();

        $carrera = Carrera::orderBy('id','asc')->get();

        // $gallery = Gallery::orderBy('id','desc')->get();

        // $events = Event::orderBy('id','desc')->get();

        // $suscription = Suscription::orderBy('id','desc')->get();

        // $inscription = Inscription::orderBy('id','desc')->get();

        // $inscripciones = Inscription::join('events','inscriptions.evento','=','events.id')
        // ->select('inscriptions.*','events.title as event_name')->take(20)
        // ->orderBy('inscriptions.id','desc')->paginate(5);


        return view('dashboard', [
            'blog' => $blog,
            'gallery' => $gallery,
            'list_blog' => $list_blog,
            'social' => $social,
            'messenger' => $messenger,
            'c_messenger' => $c_messenger,
            'carrera' => $carrera,
            // 'events' => $events,
            // 'suscription' => $suscription,
            // 'inscription' => $inscription,
            // 'inscripciones' => $inscripciones,
        ]);

    }
}
