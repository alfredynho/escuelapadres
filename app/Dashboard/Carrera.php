<?php

namespace App\Dashboard;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    protected $fillable = [
        'nombre',
        'descripcion',
        'perfil_profesional',
        'aptitudes_habilidades_destrezas',
        'objetivo_carrera',
        'objetivosxaño',
        'objetivo_trabajo',
        'pensum',
        'creador',
        'slug',
        'imagen',
        'estado',
    ];
}
