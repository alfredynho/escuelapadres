<?php

namespace App\Bot\Facebook;
use App\Messenger;

class User
{
    public static function saveUser($user)
    {
        if(Messenger::where('user_id',$user['id'])->first()){
            // No guardamos
        }else{
            // Si no Existe Guardamos
            $messenger = new Messenger();
            $messenger->user_id = $user['id'];
            $messenger->first_name = $user['first_name'];
            $messenger->last_name = $user['last_name'];
            $messenger->gender = $user['gender'];
            $messenger->timezone = $user['timezone'];
            $messenger->image = $user['profile_pic'];
            $messenger->save();
        }
    }

    public static function getUser($sender)
    {

        $client = new \GuzzleHttp\Client;

        $fields = 'id,first_name,last_name,profile_pic,gender,locale,timezone';
        // first_name,last_name,profile_pic
        // curl -X GET "https://graph.facebook.com/<PSID>?fields=first_name,last_name,profile_pic&access_token=<PAGE_ACCESS_TOKEN>"

        // https://graph.facebook.com/2038974342840249?fields=first_name,last_name,profile_pic&access_token=EAADRuExI5U0BAFRGgE1JFZAWJPgg6QKwfT3PEHaaeGwx87aB9IphZAe6OwQwpsuahAzcyQKJi4vDv3AA3RaPtA3lR2mmFOhQ2SD0UkdmujnWIywYs0aJwUTZCfbkheY8itYZCPNiWqECqHml3VZAZASpEC61ZA3yu3qwrOqGpzRtcLP1QpkZBcrX

        $profileResponse = $client->request('GET', 'https://graph.facebook.com/' . $sender . '?fields=' . $fields . '&access_token=' . env('FB_PAGE_TOKEN'));

        $user = json_decode($profileResponse->getBody(), true);

        return $user;
    }
}
