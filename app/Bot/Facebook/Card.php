<?php

namespace App\Bot\Facebook;

use Illuminate\Http\Request;
use \App\Blog;
use \App\Carrera;
use Illuminate\Support\Facades\DB;
use \App\Bot\Facebook\Serializer;

class Card
{
    public static function itemQuicReplace()
    {
        $results = [];

        $blog = Blog::orderBy('id', 'desc')->take(3)
            ->where('blogs.status', '=', '1')
            ->get();

        if (20 > 200) {
            dump("True");
            foreach ($blog as $bg) {
                $results[] = [
                    'content_type' => 'text',
                    'title' => $bg->title,
                    'payload' => $bg->author,
                    'image_url' => 'https://res.cloudinary.com/due8e2c3a/image/upload/v1559674847/alfredynho/mldjango.png'
                ];
            }
            return $results;
        } else {
            dump("false");

            $default[] = [
                'content_type' => 'text',
                'title' => "sin Registros",
                'payload' => "START",
                'image_url' => 'https://res.cloudinary.com/due8e2c3a/image/upload/v1559674847/alfredynho/mldjango.png'
            ];

            dump($default);

            return $default;
        }
    }


    public static function quickReplace($sender)
    {
        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],
                'message' => [
                    'text' => 'tienes las siguientes opciones!',
                    'quick_replies' => Card::itemQuicReplace()
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }


    public static function itemGenericTemplate($rqhome)
    {
        $results = [];

        $carreras = Carrera::orderBy('id', 'desc')->take(8)
            ->where('carreras.is_published', '=', '1')
            ->get();

        if (count($carreras) >= 1) {
            foreach ($carreras as $ca) {
                $results[] = [
                    "title" => $ca->nombre,
                    "item_url" => "https://incos.icu/carrera/sistemas-informaticos",
                    "image_url" => 'https://res.cloudinary.com/due8e2c3a/image/upload/v1573304748/INCOS%20DEV/incos-bot.webp',
                    "subtitle" => "INCOS LA PAZ.",
                    'buttons' => [
                        [
                            'type' => 'web_url',
                            'url' => $rqhome.'/carrera/'.$ca->slug,
                            'title' => 'Ver Carrera'
                        ]
                    ]
                ];
            }
            return $results;
        } else {

            $default[] = [
                "title" => "Tarjeta por Defecto",
                "item_url" => "https://incos.icu/carrera/sistemas-informaticos",
                "image_url" => 'https://res.cloudinary.com/due8e2c3a/image/upload/v1573304748/INCOS%20DEV/incos-bot.webp',
                "subtitle" => "INCOS LA PAZ.",
                'buttons' => [
                    [
                        'type' => 'web_url',
                        'url' => $rqhome,
                        'title' => 'Incos La Paz'
                    ]
                ]
            ];


            return $default;
        }
    }

    public static function genericTemplate($sender, $rqhome)
    {
        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],
                'message' => [
                    'attachment' => [
                        'type' => 'template',
                        'payload' => [
                            'template_type' => 'generic',
                            'elements' => Card::itemGenericTemplate($rqhome)
                        ]
                    ]
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }

    public static function genericTemplateIncos($sender)
    {
        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],
                'message' => [
                    'attachment' => [
                        'type' => 'template',
                        'payload' => [
                            'template_type' => 'generic',
                            'elements' => [
                                [
                                    "title" => "Carrera de Sistemas Informaticos",
                                    "item_url" => "https://incos.icu/carrera/sistemas-informaticos",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244728/slider%20incos%20bot/sistemas.png",
                                    "subtitle" => "INCOS LA PAZ.",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'https://incos.icu/carrera/sistemas-informaticos',
                                            'title' => 'Ver Carrera'
                                        ]
                                    ]
                                ],

                                [
                                    "title" => "Carrera de Secretariado Ejecutivo",
                                    "item_url" => "https://incos.icu/carrera/sistemas-informaticos",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244729/slider%20incos%20bot/secre.png",
                                    "subtitle" => "INCOS LA PAZ.",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'https://incos.icu/carrera/sistemas-informaticos',
                                            'title' => 'Ver Carrera'
                                        ]
                                    ]
                                ],

                                [
                                    "title" => "Carrera de Contaduria Publica",
                                    "item_url" => "https://incos.icu/carrera/sistemas-informaticos",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244727/slider%20incos%20bot/conta.png",
                                    "subtitle" => "INCOS LA PAZ.",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'https://incos.icu/carrera/sistemas-informaticos',
                                            'title' => 'Ver Carrera'
                                        ]
                                    ]
                                ],
                                [
                                    "title" => "Carrera de Linguistica",
                                    "item_url" => "https://incos.icu/carrera/sistemas-informaticos",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244719/slider%20incos%20bot/idiomas.png",
                                    "subtitle" => "INCOS LA PAZ.",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'https://incos.icu/carrera/sistemas-informaticos',
                                            'title' => 'Ver Carrera'
                                        ]
                                    ]
                                ],
                                [
                                    "title" => "Carrera de Comercio Internacional",
                                    "item_url" => "https://incos.icu/carrera/sistemas-informaticos",
                                    "image_url" => "https://res.cloudinary.com/due8e2c3a/image/upload/v1569244738/slider%20incos%20bot/comercio.png",
                                    "subtitle" => "INCOS LA PAZ.",
                                    'buttons' => [
                                        [
                                            'type' => 'web_url',
                                            'url' => 'https://incos.icu/carrera/sistemas-informaticos',
                                            'title' => 'Ver Carrera'
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }


    public static function listTemplate($sender)
    {

        $data = [
            'json' =>
            [
                'recipient' => ['id' => $sender],

                "message" => [
                    "attachment" => [
                        "type" => "template",
                        "payload" => [
                            "template_type" => "list",
                            "top_element_style" => "compact",
                            "elements" => [
                                [
                                    "title" => "Classic T-Shirt Collection",
                                    "subtitle" => "See all our colors",
                                    "image_url" => "https://peterssendreceiveapp.ngrok.io/img/collection.png",
                                    "buttons" => [
                                        [
                                            "title" => "View",
                                            "type" => "web_url",
                                            "url" => "https://peterssendreceiveapp.ngrok.io/collection",
                                            "messenger_extensions" => true,
                                            "webview_height_ratio" => "tall",
                                            "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                        ]
                                    ]
                                ],
                                [
                                    "title" => "Classic White T-Shirt",
                                    "subtitle" => "See all our colors",
                                    "default_action" => [
                                        "type" => "web_url",
                                        "url" => "https://peterssendreceiveapp.ngrok.io/view?item=100",
                                        "messenger_extensions" => false,
                                        "webview_height_ratio" => "tall"
                                    ]
                                ],
                                [
                                    "title" => "Classic Blue T-Shirt",
                                    "image_url" => "https://peterssendreceiveapp.ngrok.io/img/blue-t-shirt.png",
                                    "subtitle" => "100% Cotton, 200% Comfortable",
                                    "default_action" => [
                                        "type" => "web_url",
                                        "url" => "https://peterssendreceiveapp.ngrok.io/view?item=101",
                                        "messenger_extensions" => true,
                                        "webview_height_ratio" => "tall",
                                        "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                    ],
                                    "buttons" => [
                                        [
                                            "title" => "Shop Now",
                                            "type" => "web_url",
                                            "url" => "https://peterssendreceiveapp.ngrok.io/shop?item=101",
                                            "messenger_extensions" => true,
                                            "webview_height_ratio" => "tall",
                                            "fallback_url" => "https://peterssendreceiveapp.ngrok.io/"
                                        ]
                                    ]
                                ]
                            ],
                            "buttons" => [
                                [
                                    "title" => "View More",
                                    "type" => "postback",
                                    "payload" => "payload"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;
    }
}
