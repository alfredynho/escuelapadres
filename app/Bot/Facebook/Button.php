<?php

namespace App\Bot\Facebook;

use Illuminate\Http\Request;
use \App\Blog;
use Illuminate\Support\Facades\DB;
use \App\Bot\Facebook\Serializer;

class Button
{

    public static function templateButton($sender){

        $data = ['json' =>
            [
            "recipient" => [
                "id" => $sender
            ],

            "message" => [
                "attachment" => [
                "type" => "template",
                "payload" => [
                    "template_type" => "button",
                    "text" => "What do you want to do next?",
                    "buttons" => [
                    [
                        "type" => "web_url",
                        "url" => "https://www.messenger.com",
                        "title" => "Visit Messenger"
                    ],
                    [
                        "type" => "web_url",
                        "url" => "https://www.messenger.com",
                        "title" => "Visit Messenger"
                    ]
                    ]
                ]
                ]
            ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;

    }

    public static function callButton($sender)
    {

        $data = ['json' =>
            [
            "recipient" => [
                "id" => $sender
            ],

            "message" => [
                "attachment" => [
                  "type" => "template",
                  "payload" => [
                    "template_type" => "button",
                    "text" => "¿Necesita más ayuda? Hable con un representante",
                    "buttons" => [
                      [
                        "type" => "phone_number",
                        "title" => "Call Representative",
                        "payload" => "+59173053045"
                      ]
                    ]
                  ]
                ]
              ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;

    }


    public static function shareButton($sender)
    {

        $data = ['json' =>
            [
            "recipient" => [
                "id" => $sender
            ],


            "message" => [
                "attachment" => [
                  "type" => "template",
                  "payload" => [
                    "template_type" => "generic",
                    "elements" => [
                      [
                        "title" => "Breaking News: Record Thunderstorms",
                        "subtitle" => "The local area is due for record thunderstorms over the weekend.",
                        "image_url" => "https://miro.medium.com/max/3000/1*H4YNY3rPbfy1WLyYlR6PgQ.jpeg",
                        "buttons" => [
                          [
                            "type" => "element_share",
                            "share_contents" => [
                              "attachment" => [
                                "type" => "template",
                                "payload" => [
                                  "template_type" => "generic",
                                  "elements" => [
                                    [
                                      "title" => "I took the hat quiz",
                                      "subtitle" => "My result: Fez",
                                      "image_url" => "https://miro.medium.com/max/3000/1*H4YNY3rPbfy1WLyYlR6PgQ.jpeg",
                                      "default_action" => [
                                        "type" => "web_url",
                                        "url" => "http://m.me/petershats?ref=invited_by_24601"
                                    ],
                                      "buttons" => [
                                        [
                                          "type" =>"web_url",
                                          "url" => "http://m.me/petershats?ref=invited_by_24601",
                                          "title" => "Take Quiz"
                                          ]
                                      ]
                                    ]
                                  ]
                                ]
                              ]
                            ]
                          ]
                        ]
                      ]
                    ]
                  ]
                ]
              ]
            ]
        ];

        $response = Serializer::parseResponse($data);
        return $response;

    }

}
