<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    protected $fillable = [
        'user',
        'fistname',
        'paterno',
        'materno',
        'cedula',
        'email',
        'phone',
        'id_inscription'
    ];


}
