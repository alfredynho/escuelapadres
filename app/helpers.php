<?php

use Carbon\Carbon;
use App\Carrera;
use App\Social;
use App\Question;

//Funcion para generar nombre a imagenes
function generaCode()
{
    $uniqid = uniqid();
    $rand_start = rand(1,5);
    $name_image = substr($uniqid,$rand_start,8);

    return $name_image;
}

// Function para formatear fecha
function formaterDate($date)
{
    $form_date = Carbon::createFromFormat('m-d-Y', $date);
    $format_date = $form_date->format('Y-m-d');

    return $format_date;
}


// Function para sacar url de la red social
function getSocial($name)
{
    try {
        $url_social = Social::where('name', $name)->firstOrFail();

        if ($url_social->status == 1) {
            return $url_social->url;
        } else {
            return "https://escuelapadres.com/";
        }

    } catch (\Exception $error) {
        return "https://escuelapadres.com/";
    }
}


// Function para sacar todas las carreras y que viva en todo el proyecto y tambien para consulta en
// en el menu de navegacion Navigation ->take(10) para solo sacar los primeros 10 registros de la base de datos
function getCarreras()
{
    $carreras = Carrera::orderBy('id', 'desc')->get()->take(10);
    return $carreras;

}

//Funcion mes literal para la pagina de inicio -> Seccion Noticias
function mesLiteral($month)
{
    switch ($month) {
        case "1":
            return "Enero";
            break;
        case "2":
            echo "Febrero";
            break;
        case "3":
            echo "Marzo";
            break;
        case "4":
            echo "Abril";
            break;
        case "5":
            echo "Mayo";
            break;
        case "6":
            echo "Junio";
            break;
        case "7":
            echo "Julio";
            break;
        case "8":
            echo "Agosto";
            break;
        case "9":
            echo "Septiembre";
            break;
        case "10":
            echo "Octubre";
            break;
        case "11":
            echo "Noviembre";
            break;
        case "12":
            echo "Diciembre";
            break;

        default:
            echo "Mes";
    }
}


function randomColor()
{
    $colors = array('warning','success','danger','info','primary');

    return $colors[array_rand($colors)];
}


function cantidadRegFaqs($id){

    $c_questions = Question::where('action_id','=',$id)->count();

    return $c_questions;
}
