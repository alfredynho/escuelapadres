<?php
// Ruta de pagina de inicio
Route::get('/','EscuelaController@index')->name('landing');

Route::get('/nosotros','EscuelaController@nosotros')->name('nosotros');
Route::get('/cursos','EscuelaController@escuela')->name('escuela');

//Route::post('/contactos', 'Dashboard\ContactController@store')->name('dashboard.contactos.store');

Route::get('/estructura','EscuelaController@autoridades')->name('structure');
Route::get('/inscripcion','EscuelaController@inscripcion')->name('inscripciones');
Route::get('/contactos','EscuelaController@contactos')->name('contactos');

Route::post('/save','EscuelaController@store')->name('inscription.store');


Auth::routes();

Route::get('/dashboard/messenger','Dashboard\MessengerController@index')->name('admin-messenger')->middleware('auth');
Route::delete('/dashboard/messenger/{id}','Dashboard\MessengerController@destroy')->name('admin-messenger-destroy')->middleware('auth');

Route::get('/dashboard', 'Dashboard\DashboardController@index')->name('dashboard')->middleware('auth');

Route::get('/dashboard/blog', 'Dashboard\BlogController@index')->name('dashboard.blog.index')->middleware('auth');
Route::delete('/dashboard/blog/{id}', 'Dashboard\BlogController@destroy')->name('dashboard.blog.destroy')->middleware('auth');
Route::put('/dashboard/blog/{id}','Dashboard\BlogController@update')->name('dashboard.blog.update')->middleware('auth');
Route::post('/dashboard/blog', 'Dashboard\BlogController@store')->name('dashboard.blog.store')->middleware('auth');

Route::get('/clear',function(){
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');

    return '<h1> Caches limpias </h1>';
});
